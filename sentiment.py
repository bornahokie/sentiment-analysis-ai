import os
import chardet
import string

# This example requires environment variables named "LANGUAGE_KEY" and "LANGUAGE_ENDPOINT"
language_key = os.environ.get('LANGUAGE_KEY')
language_endpoint = os.environ.get('LANGUAGE_ENDPOINT')

from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential

# Authenticate the client using your key and endpoint 
def authenticate_client():
    ta_credential = AzureKeyCredential(language_key)
    text_analytics_client = TextAnalyticsClient(
            endpoint=language_endpoint, 
            credential=ta_credential)
    return text_analytics_client

client = authenticate_client()

def filter_text(text):
    # Define the set of characters you want to keep
    valid_characters = set(string.ascii_letters + string.digits + string.punctuation + ' ')

    # Filter out characters that are not in the valid set
    filtered_text = ''.join(char for char in text if char in valid_characters)

    return filtered_text

# Example method for detecting sentiment and opinions in text 
def sentiment_analysis_with_opinion_mining_example(client):

    for i in range(1, 11):
        documents = []
        with open("filtered_ai" + str(i) + ".txt", 'rb') as det:
            result = chardet.detect(det.read())
        with open("filtered_ai" + str(i) + ".txt", 'r', encoding = result['encoding'], errors = 'ignore') as f:
            data = f.read()
            documents.append(data)

            result = client.analyze_sentiment(documents, show_opinion_mining=True)
            doc_result = [doc for doc in result if not doc.is_error]

            positive_reviews = [doc for doc in doc_result if doc.sentiment == "positive"]
            negative_reviews = [doc for doc in doc_result if doc.sentiment == "negative"]

            positive_mined_opinions = []
            mixed_mined_opinions = []
            negative_mined_opinions = []

            for document in doc_result:
                with open("sentiment_output" + str(i) + ".txt", 'w', encoding="utf-8") as output_file:
                    output_file.write("Document Sentiment: {}".format(document.sentiment))
                    output_file.write("\n")
                    output_file.write("Overall scores: positive={0:.2f}; neutral={1:.2f}; negative={2:.2f} \n".format(
                        document.confidence_scores.positive,
                        document.confidence_scores.neutral,
                        document.confidence_scores.negative,
                    ))
                    output_file.write("\n")
                    for sentence in document.sentences:
                        output_file.write("Sentence: {}".format(sentence.text))
                        output_file.write("\n")
                        output_file.write("Sentence sentiment: {}".format(sentence.sentiment))
                        output_file.write("\n")
                        output_file.write("Sentence score:\nPositive={0:.2f}\nNeutral={1:.2f}\nNegative={2:.2f}\n".format(
                            sentence.confidence_scores.positive,
                            sentence.confidence_scores.neutral,
                            sentence.confidence_scores.negative,
                        ))
                        output_file.write("\n")
                        for mined_opinion in sentence.mined_opinions:
                            target = mined_opinion.target
                            output_file.write("......'{}' target '{}'".format(target.sentiment, target.text))
                            output_file.write("\n")
                            output_file.write("......Target score:\n......Positive={0:.2f}\n......Negative={1:.2f}\n".format(
                                target.confidence_scores.positive,
                                target.confidence_scores.negative,
                            ))
                            output_file.write("\n")
                            for assessment in mined_opinion.assessments:
                                output_file.write("......'{}' assessment '{}'".format(assessment.sentiment, assessment.text))
                                output_file.write("\n")
                                output_file.write("......Assessment score:\n......Positive={0:.2f}\n......Negative={1:.2f}\n".format(
                                    assessment.confidence_scores.positive,
                                    assessment.confidence_scores.negative,
                                ))
                        output_file.write("\n")
                    output_file.write("\n")
          
sentiment_analysis_with_opinion_mining_example(client)