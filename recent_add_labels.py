import json
import chardet

positive_words = set()
negative_words = set()

# Read positive words from the file "positive.txt"
with open("positive.txt", 'r', encoding='utf-8') as positive_file:
    positive_words.update(word.strip().casefold() for word in positive_file)

# Read negative words from the file "negative.txt"
with open("negative.txt", 'r', encoding='utf-8') as negative_file:
    negative_words.update(word.strip().casefold() for word in negative_file)

json_data = {
    "projectFileVersion": "2022-05-01",
    "stringIndexType": "Utf16CodeUnit",
    "metadata": {
        "projectKind": "CustomEntityRecognition",
        "storageInputContainerName": "example-data",
        "projectName": "dcc-capstone",
        "multilingual": "false",
        "description": "Project-description",
        "language": "en",
        "settings": {}
    },
    "assets": {
        "projectKind": "CustomEntityRecognition",
        "entities": [
            {"category": "positive"},
            {"category": "negative"},
            {"category": "percentage"},
            {"category": "demographic"}
        ],
        "documents": []
    }
}
for i in range(1, 11):
    with open("ai" + str(i) + ".txt", 'rb') as det:
        result = chardet.detect(det.read())
    #with open(str(i) + ".txt", 'r', encoding="utf-8") as f:
    with open("ai" + str(i) + ".txt", 'r', encoding = result['encoding'], errors = 'ignore') as f:
        utf_data = f.read()
        region_length = len(utf_data)
        category = ""
        offset = 0
        length = 0
        last_position = 0
        is_a_category = False

        document_structure = {
            "location": "ai" + str(i) + ".txt",
            "language": "en-US",
            "entities": [
                {
                    "regionOffset": 0,
                    "regionLength": region_length,
                    "labels": []
                }
            ]
        }
        for ind, word in enumerate(utf_data.split()):
            #if word.casefold().__contains__("positive") or word.casefold().__contains__("excite") or word.casefold().__contains__("benefit") or word.casefold().__contains__("beneficial") or word.casefold().__contains__("favor") or word.casefold().__contains__("good") or word.casefold().__contains__("optimist") or word.casefold().__contains__("favor"):
            if word.casefold() in positive_words:  
                category = "positive"
                offset = utf_data.find(word, last_position)
                length = len(word)
                last_position = offset + length
                is_a_category = True
            #elif word.casefold().__contains__("negative") or word.casefold().__contains__("concern") or word.casefold().__contains__("harm") or word.casefold().__contains__("reject") or word.casefold().__contains__("oppose") or word.casefold().__contains__("danger") or word.casefold().__contains__("bad") or word.casefold().__contains__("pessimist") or word.casefold().__contains__("risk") or word.casefold().__contains__("threat"):
            elif word.casefold() in negative_words:    
                category = "negative"
                offset = utf_data.find(word, last_position)
                length = len(word)
                last_position = offset + length
                is_a_category = True
            elif word.casefold().__contains__("%") or word.casefold().__contains__("half") or word.casefold().__contains__("third") or word.casefold().__contains__("fourth") or word.casefold().__contains__("fifth") or word.casefold().__contains__("tenth"):
                category = "percentage"
                offset = utf_data.find(word, last_position)
                length = len(word)
                last_position = offset + length
                is_a_category = True
            elif word.casefold().__contains__("American") or word.casefold().__contains__("white") or word.casefold().__contains__("black") or word.casefold().__contains__("asian") or word.casefold().__contains__("hispanic") or word.casefold() == "women" or word.casefold() == "men":
                category = "demographic"
                offset = utf_data.find(word, last_position)
                length = len(word)
                last_position = offset + length
                is_a_category = True

            if is_a_category:
                document_structure["entities"][0]["labels"].append({
                    "category": category,
                    "offset": offset,
                    "length": length
                })
                is_a_category = False
            
        json_data["assets"]["documents"].append(document_structure)

        # Move the writing of JSON data outside the loop
        with open('entity_labels.json', 'w') as file:
            json.dump(json_data, file, indent=4)      
