for i in range(1, 7):
    with open(str(i) + ".txt", 'r', encoding="utf-8") as f:
        utf_data = f.read()
        region_length = len(utf_data)
        category = ""
        offset = 0
        length = 0
        last_position = 0
        is_a_category = False
            
        positives = set(open('positive.txt').read().split())
        negatives = set(open('negative.txt').read().split())

        c = len(utf_data.split())
        p = len([w for w in utf_data.split() if w in positives])
        n = len([w for w in utf_data.split() if w in negatives])
        print((p-n)/c)
        with open('output.txt', 'a+') as f:
            f.seek(0)
            data = f.read(100)
            if(len(data) > 0):
                f.write('\n')
            f.write(str(c) + ',' + str(p) + ',' + str(n))